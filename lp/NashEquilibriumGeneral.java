package lp;

import java.util.ArrayList;

import com.winvector.linalg.DenseVec;
import com.winvector.linalg.Matrix;
import com.winvector.linalg.colt.NativeMatrix;
import com.winvector.lp.LPEQProb;
import com.winvector.lp.LPException;
import com.winvector.lp.LPException.LPMalformedException;


public class NashEquilibriumGeneral {

	public static void nashEquilibriumExample() throws LPException {
		NormalFormProblem problem = exampleNormalFormProblem();	
		solveNashEquilibrium(problem);		
	}

	public static NormalFormProblem exampleNormalFormProblem(){
		NormalFormProblem problem = new NormalFormProblem();
		problem.rowMoves = new ArrayList<String>();
		problem.colMoves = new ArrayList<String>();	

		problem.rowMoves.add("1");
		problem.rowMoves.add("2");
		problem.rowMoves.add("3");
		problem.colMoves.add("1");
		problem.colMoves.add("2");

		double[][] u2 = new double[3][2];
		u2[0][0] = 3.0; u2[0][1] = 2.0;
		u2[1][0] = 2.0; u2[1][1] = 6.0;
		u2[2][0] = 3.0; u2[2][1] = 1.0;
		double[][] u1 = new double[3][2];
		u1[0][0] = 3.0; u1[0][1] = 3.0;
		u1[1][0] = 2.0; u1[1][1] = 5.0;
		u1[2][0] = 0.0; u1[2][1] = 6.0;

		problem.u1 = u1;
		problem.u2 = u2;

		return problem;

	}

	public static void showNormalFormProblem(NormalFormProblem problem) {
		System.out.print("****");
		for (int j = 0; j<problem.pCol.length; j++)  if (problem.pCol[j]) 
			System.out.print("***********");
		System.out.println();
		System.out.print("  ");
		for (int j = 0; j<problem.pCol.length; j++)  if (problem.pCol[j]) {
			if (problem.colMoves.size()>0) {
				System.out.print("      ");
				System.out.print(problem.colMoves.get(j));
				System.out.print("    ");
			}
			else {
				System.out.print("\t");
				System.out.print("Col " +j);
			}
		}
		System.out.println();
		for (int i = 0; i<problem.pRow.length; i++) if (problem.pRow[i]) {
			if (problem.rowMoves.size()>0) System.out.print(problem.rowMoves.get(i)+ ": ");
			else System.out.print("Row " +i+ ": ");
			for (int j = 0; j<problem.pCol.length; j++)  if (problem.pCol[j]) {
				String fs = String.format("| %3.0f,%3.0f", problem.u1[i][j], problem.u2[i][j]);
				System.out.print(fs+"  ");
			}
			System.out.println("|");
		}
		System.out.print("****");
		for (int j = 0; j<problem.pCol.length; j++)  if (problem.pCol[j]) 
			System.out.print("***********");
		System.out.println();
	}

	public static double[][] transpose(double[][] u) {
		int lin = u.length;
		int col = u[0].length;
		double[][] t = new double[col][lin];
		for (int i = 0; i<lin; i++)
			for (int j = 0; j<col; j++) t[j][i] = u[i][j];
		return t;
	}

	public static void solveNashEquilibrium(NormalFormProblem problem) throws LPException {
		solveLP(problem.u1, problem.u2, problem.rowMoves.size(), problem.colMoves.size());
	}

	public static void solveLP(double[][] u1, double[][] u2, int rows, int cols) throws LPException {

		LinearProgrammingProblem problem = new LinearProgrammingProblem();

		problem.numVariables = cols + 1 + rows + 1;   // number of variables
		problem.numConstraints = rows + 1 + cols + 1; // number of constraints

		final Matrix<NativeMatrix> m = NativeMatrix.factory.newMatrix(problem.numConstraints,problem.numVariables+problem.numConstraints,false);
		final double[] b = new double[problem.numConstraints];
		final double[] c = new double[problem.numVariables+problem.numConstraints];


		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < cols; j++) { 
				m.set(i, j, u1[i][j]);
			}
			m.set(i, cols, -1.0);
		}
		for(int j = 0; j < cols; j++) {
			m.set(rows, j, 1.0);
		}
		c[cols] = 1.0;
		b[rows] = 1.0;
		
		u2 = transpose(u2);
		
		for(int i = rows+1; i < rows+1+cols; i++) {
			for(int j = cols+1; j < cols+1+rows; j++) { 
				m.set(i, j, u2[i-rows-1][j-cols-1]);
			}
			m.set(i, cols+1+rows, -1.0);
		}
		for(int j = cols+1; j < cols+1+rows; j++) {
			m.set(rows+1+cols, j, 1.0);
		}
		c[cols+1+rows] = 1.0;
		b[rows+1+cols] = 1.0;
		

		problem.m = m;
		problem.b = b;
		problem.c = c;
		problem.prob = null;
		try {
			problem.prob = new LPEQProb(m.columnMatrix(),b,new DenseVec(c));
		}
		catch (LPMalformedException e) {
			System.out.println("Error in problem specification!");
			e.printStackTrace();
		}
		LinearProgramming.showProblem(problem);
		LinearProgramming.solveProblem(problem);

		if (problem.feasible) {
			if (problem.bounded) {
				LinearProgramming.showSolution(problem);
			}
			else System.out.println("The problem is unbounded!");
		}
		else System.out.println("The problem is infeasible!");

	}
}




