package lp;

import java.util.ArrayList;
import java.util.List;

import com.winvector.linalg.DenseVec;
import com.winvector.linalg.Matrix;
import com.winvector.linalg.colt.NativeMatrix;
import com.winvector.lp.LPEQProb;
import com.winvector.lp.LPException;
import com.winvector.lp.LPException.LPMalformedException;


public class NashEquilibriumGeneralSets {

	public static void nashEquilibriumExample() throws LPException {
		NormalFormProblem problem = exampleNormalFormProblem();	
		solveNashEquilibrium(problem);		
	}
	
	public static List<double[]> solveNashEquilibriumStrategy(int[][] u1, int[][] u2, List<boolean[]> moves) throws LPException {
		NormalFormProblem problem = setNormalFormProblem(u1, u2, moves);
		double[] solution = solveNashEquilibrium(problem);
		List<double[]> equilibria = new ArrayList<double[]>();
		double[] equilibriaP1 = new double[u1.length];
		double[] equilibriaP2 = new double[u1[0].length];
		int rows = u1[0].length + 1;
		for(int i = 0; i < equilibriaP1.length; i++) {
			if(moves.get(0)[i]) {
				equilibriaP1[i] = solution[rows];
				rows++;
			}
			else
				equilibriaP1[i] = 0.0;
		}
		int cols = 0;
		for(int i = 0; i < equilibriaP2.length; i++) {
			if(moves.get(1)[i]) {
				equilibriaP2[i] = solution[cols];
				cols++;
			}
			else
				equilibriaP2[i] = 0.0;
		}
		equilibria.add(equilibriaP1);
		equilibria.add(equilibriaP2);
		
		return equilibria;
	}
	
	public static NormalFormProblem setNormalFormProblem(int[][] u1, int[][] u2, List<boolean[]> moves) {
		NormalFormProblem problem = new NormalFormProblem();
		problem.rowMoves = new ArrayList<String>();
		problem.colMoves = new ArrayList<String>();	
		
		int nRows = 0;
		int nCols = 0;
		
		for(int i = 0; i < moves.get(0).length; i++) {
			if(moves.get(0)[i])
				nRows++;
		}
		for(int i = 0; i < moves.get(1).length; i++) {
			if(moves.get(1)[i])
				nCols++;
		}
			

		for(int i = 1; i <= nRows; i++)
			problem.rowMoves.add(Integer.toString(i));
		for(int i = 1; i <= nCols; i++)
			problem.colMoves.add(Integer.toString(i));

		double[][] doubleU1 = new double[nRows][nCols];

		for(int i = 0; i < u1.length; i++) {
			if(moves.get(0)[i]) {
				for(int j = 0; j < u1[0].length; j++) {
					if(moves.get(1)[j])
						doubleU1[i][j] = u1[i][j];
				}
			}
		}

		double[][] doubleU2 = new double[nRows][nCols];

		for(int i = 0; i < u2.length; i++) {
			if(moves.get(0)[i]) {
				for(int j = 0; j < u2[0].length; j++) {
					if(moves.get(1)[j])
						doubleU2[i][j] = u2[i][j];
				}
			}
		}

		problem.u1 = doubleU1;
		problem.u2 = doubleU2;

		return problem;
	}

	public static NormalFormProblem exampleNormalFormProblem(){
		NormalFormProblem problem = new NormalFormProblem();
		problem.rowMoves = new ArrayList<String>();
		problem.colMoves = new ArrayList<String>();	

		problem.rowMoves.add("1");
		problem.rowMoves.add("2");
		problem.rowMoves.add("3");
		problem.colMoves.add("1");
		problem.colMoves.add("2");

		double[][] u2 = new double[3][2];
		u2[0][0] = 3.0; u2[0][1] = 2.0;
		u2[1][0] = 2.0; u2[1][1] = 6.0;
		u2[2][0] = 3.0; u2[2][1] = 1.0;
		double[][] u1 = new double[3][2];
		u1[0][0] = 3.0; u1[0][1] = 3.0;
		u1[1][0] = 2.0; u1[1][1] = 5.0;
		u1[2][0] = 0.0; u1[2][1] = 6.0;

		problem.u1 = u1;
		problem.u2 = u2;

		return problem;

	}

	public static void showNormalFormProblem(NormalFormProblem problem) {
		System.out.print("****");
		for (int j = 0; j<problem.pCol.length; j++)  if (problem.pCol[j]) 
			System.out.print("***********");
		System.out.println();
		System.out.print("  ");
		for (int j = 0; j<problem.pCol.length; j++)  if (problem.pCol[j]) {
			if (problem.colMoves.size()>0) {
				System.out.print("      ");
				System.out.print(problem.colMoves.get(j));
				System.out.print("    ");
			}
			else {
				System.out.print("\t");
				System.out.print("Col " +j);
			}
		}
		System.out.println();
		for (int i = 0; i<problem.pRow.length; i++) if (problem.pRow[i]) {
			if (problem.rowMoves.size()>0) System.out.print(problem.rowMoves.get(i)+ ": ");
			else System.out.print("Row " +i+ ": ");
			for (int j = 0; j<problem.pCol.length; j++)  if (problem.pCol[j]) {
				String fs = String.format("| %3.0f,%3.0f", problem.u1[i][j], problem.u2[i][j]);
				System.out.print(fs+"  ");
			}
			System.out.println("|");
		}
		System.out.print("****");
		for (int j = 0; j<problem.pCol.length; j++)  if (problem.pCol[j]) 
			System.out.print("***********");
		System.out.println();
	}

	public static double[][] transpose(double[][] u) {
		int lin = u.length;
		int col = u[0].length;
		double[][] t = new double[col][lin];
		for (int i = 0; i<lin; i++)
			for (int j = 0; j<col; j++) t[j][i] = u[i][j];
		return t;
	}

	public static double[] solveNashEquilibrium(NormalFormProblem problem) throws LPException {
		double[] solution;
		LinearProgrammingProblem problemLP;
		
		problemLP = solveLP(problem.u1, problem.u2, problem.rowMoves.size(), problem.colMoves.size());
		LinearProgramming.showProblem(problemLP);
		LinearProgramming.solveProblem(problemLP);

		if (problemLP.feasible) {
			if (problemLP.bounded) {
				LinearProgramming.showSolution(problemLP);
				solution = LinearProgramming.getSolution(problemLP);
				return solution;
			}
			else System.out.println("The problem is unbounded!");
		}
		else System.out.println("The problem is infeasible!");
		
		List<boolean[]> pRows;
		List<boolean[]> pCols;
		if(problem.rowMoves.size() > 2) {
			pRows = SubSets.getSubSets(0,2,problem.rowMoves.size());
			if(problem.colMoves.size() > 2) {
				pCols = SubSets.getSubSets(0,2,problem.colMoves.size());
				for(boolean[] subSetRow: pRows) {
					for(boolean[] subSetCol: pCols) {
						problemLP = solve2SetLP(problem.u1, problem.u2, problem.rowMoves.size(), problem.colMoves.size(), subSetRow, subSetCol);
						LinearProgramming.showProblem(problemLP);
						LinearProgramming.solveProblem(problemLP);
						if (problemLP.feasible) {
							if (problemLP.bounded) {
								LinearProgramming.showSolution(problemLP);
								solution = LinearProgramming.getSolution(problemLP);
								return solution;
							}
							else System.out.println("The problem is unbounded!");
						}
						else System.out.println("The problem is infeasible!");
					}
				}
			}
			else {
				for(boolean[] subSetRow: pRows) {
					problemLP = solveSetLP(problem.u1, problem.u2, problem.rowMoves.size(), problem.colMoves.size(), subSetRow, true);
					LinearProgramming.showProblem(problemLP);
					LinearProgramming.solveProblem(problemLP);
					if (problemLP.feasible) {
						if (problemLP.bounded) {
							LinearProgramming.showSolution(problemLP);
							solution = LinearProgramming.getSolution(problemLP);
							return solution;
						}
						else System.out.println("The problem is unbounded!");
					}
					else System.out.println("The problem is infeasible!");
				}
			}
		}
		if(problem.colMoves.size() > 2) {
			pCols = SubSets.getSubSets(0,2,problem.colMoves.size());
			for(boolean[] subSetCol: pCols) {
				problemLP = solveSetLP(problem.u1, problem.u2, problem.rowMoves.size(), problem.colMoves.size(), subSetCol, false);
				LinearProgramming.showProblem(problemLP);
				LinearProgramming.solveProblem(problemLP);
				if (problemLP.feasible) {
					if (problemLP.bounded) {
						LinearProgramming.showSolution(problemLP);
						solution = LinearProgramming.getSolution(problemLP);
						return solution;
					}
					else System.out.println("The problem is unbounded!");
				}
				else System.out.println("The problem is infeasible!");
			}
		}
		return null;
	}
	
	public static LinearProgrammingProblem solve2SetLP(double[][] u1, double[][] u2, int rows, int cols, boolean[] subSetRow, boolean[] subSetCol) throws LPException {

		LinearProgrammingProblem problem = new LinearProgrammingProblem();
		
		int nonSup = 0;
		for (boolean x: subSetRow)
			if(x == false)
				nonSup++;
		for (boolean x: subSetCol)
			if(x == false)
				nonSup++;

		problem.numVariables = cols + 1 + rows + 1;   // number of variables
		problem.numConstraints = rows + 1 + cols + 1 + nonSup; // number of constraints
			
		
		final Matrix<NativeMatrix> m = NativeMatrix.factory.newMatrix(problem.numConstraints,problem.numVariables+problem.numConstraints,false);
		final double[] b = new double[problem.numConstraints];
		final double[] c = new double[problem.numVariables+problem.numConstraints];


		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < cols; j++) { 
				m.set(i, j, u1[i][j]);
			}
			m.set(i, cols, -1.0);
			if(subSetCol[i])
				m.set(i, cols+1+rows+1+i, 1.0);
		}
		
		for(int j = 0; j < cols; j++) {
			if(subSetCol[j])
				m.set(rows, j, 1.0);
		}
		
		c[cols] = 1.0;
		b[rows] = 1.0;
		
		u2 = transpose(u2);
		
		for(int i = rows+1; i < rows+1+cols; i++) {
			for(int j = cols+1; j < cols+1+rows; j++) { 
				m.set(i, j, u2[i-rows-1][j-cols-1]);
			}
			m.set(i, cols+1+rows, -1.0);
			if(subSetRow[i-rows-1])
				m.set(i, cols+1+rows+1+rows+1+i, 1.0);
		}
		for(int j = cols+1; j < cols+1+rows; j++) {
			if(subSetRow[j-cols-1])
				m.set(rows+1+cols, j, 1.0);
		}
				
		c[cols+1+rows] = 1.0;
		b[rows+1+cols] = 1.0;
		
		int addedColConstraints = 0;
		for (int i = 0; i < subSetCol.length; i++)
			if(!subSetCol[i]) {
				m.set(rows+1+cols+1+addedColConstraints, i, 1.0);
				addedColConstraints++;
			}
		int addedRowConstraints = 0;
		for (int i = 0; i < subSetRow.length; i++)
			if(!subSetRow[i]) {
				m.set(rows+1+cols+1+addedColConstraints+addedRowConstraints, cols+1+i, 1.0);
				addedRowConstraints++;
			}
		

		problem.m = m;
		problem.b = b;
		problem.c = c;
		problem.prob = null;
		try {
			problem.prob = new LPEQProb(m.columnMatrix(),b,new DenseVec(c));
		}
		catch (LPMalformedException e) {
			System.out.println("Error in problem specification!");
			e.printStackTrace();
		}

		return problem;

	}

	public static LinearProgrammingProblem solveSetLP(double[][] u1, double[][] u2, int rows, int cols, boolean[] subSet, boolean isRow) throws LPException {

		LinearProgrammingProblem problem = new LinearProgrammingProblem();
		
		int nonSup = 0;
		for (boolean x: subSet)
			if(x == false)
				nonSup++;

		problem.numVariables = cols + 1 + rows + 1;   // number of variables
		problem.numConstraints = rows + 1 + cols + 1 + nonSup; // number of constraints
			
		
		final Matrix<NativeMatrix> m = NativeMatrix.factory.newMatrix(problem.numConstraints,problem.numVariables+problem.numConstraints,false);
		final double[] b = new double[problem.numConstraints];
		final double[] c = new double[problem.numVariables+problem.numConstraints];


		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < cols; j++) { 
				m.set(i, j, u1[i][j]);
			}
			m.set(i, cols, -1.0);
			if(isRow)
				if(!subSet[i])
					m.set(i, cols+1+rows+1+i, 1.0);
		}
		for(int j = 0; j < cols; j++) {
			if(!isRow) {
				if(subSet[j])
					m.set(rows, j, 1.0);
			}
			else
				m.set(rows, j, 1.0);
		}
		c[cols] = 1.0;
		b[rows] = 1.0;
		
		u2 = transpose(u2);
		
		for(int i = rows+1; i < rows+1+cols; i++) {
			for(int j = cols+1; j < cols+1+rows; j++) { 
				m.set(i, j, u2[i-rows-1][j-cols-1]);
			}
			m.set(i, cols+1+rows, -1.0);
			if(!isRow)
				if(!subSet[i-rows-1]) {
					System.out.println("added slack");
					m.set(i, cols+1+rows+1+rows+1+i, 1.0);
				}
		}
		for(int j = cols+1; j < cols+1+rows; j++) {
			if(isRow) {
				if(subSet[j-cols-1])
					m.set(rows+1+cols, j, 1.0);
			}
			else
				m.set(rows+1+cols, j, 1.0);
		}
		
		c[cols+1+rows] = 1.0;
		b[rows+1+cols] = 1.0;
		
		int addedConstraints = 0;
		if(!isRow) {
		for (int i = 0; i < subSet.length; i++)
			if(!subSet[i]) {
				m.set(rows+1+cols+1+addedConstraints, i, 1.0);
				addedConstraints++;
			}
		
		}
		else {
		for (int i = 0; i < subSet.length; i++)
			if(!subSet[i]) {
				m.set(rows+1+cols+1+addedConstraints, cols+1+i, 1.0);
				addedConstraints++;
			}
		}
		
		problem.m = m;
		problem.b = b;
		problem.c = c;
		problem.prob = null;
		try {
			problem.prob = new LPEQProb(m.columnMatrix(),b,new DenseVec(c));
		}
		catch (LPMalformedException e) {
			System.out.println("Error in problem specification!");
			e.printStackTrace();
		}

		return problem;

	}
	
	public static LinearProgrammingProblem solveLP(double[][] u1, double[][] u2, int rows, int cols) throws LPException {

		LinearProgrammingProblem problem = new LinearProgrammingProblem();

		problem.numVariables = cols + 1 + rows + 1;   // number of variables
		problem.numConstraints = rows + 1 + cols + 1; // number of constraints
			
		
		final Matrix<NativeMatrix> m = NativeMatrix.factory.newMatrix(problem.numConstraints,problem.numVariables+problem.numConstraints,false);
		final double[] b = new double[problem.numConstraints];
		final double[] c = new double[problem.numVariables+problem.numConstraints];


		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < cols; j++) { 
				m.set(i, j, u1[i][j]);
			}
			m.set(i, cols, -1.0);
		}
		for(int j = 0; j < cols; j++) {
			m.set(rows, j, 1.0);
		}
		c[cols] = 1.0;
		b[rows] = 1.0;
		
		u2 = transpose(u2);
		
		for(int i = rows+1; i < rows+1+cols; i++) {
			for(int j = cols+1; j < cols+1+rows; j++) { 
				m.set(i, j, u2[i-rows-1][j-cols-1]);
			}
			m.set(i, cols+1+rows, -1.0);
		}
		for(int j = cols+1; j < cols+1+rows; j++) {
			m.set(rows+1+cols, j, 1.0);
		}
		c[cols+1+rows] = 1.0;
		b[rows+1+cols] = 1.0;
		

		problem.m = m;
		problem.b = b;
		problem.c = c;
		problem.prob = null;
		try {
			problem.prob = new LPEQProb(m.columnMatrix(),b,new DenseVec(c));
		}
		catch (LPMalformedException e) {
			System.out.println("Error in problem specification!");
			e.printStackTrace();
		}
		
		return problem;

	}
}




