package lp;

import java.util.Iterator;

import gametree.GameNode;

public final class MinMax {

	public static Result maxStrat(GameNode node){
		if(node.isTerminal()){
			return new Result(node.getLabel(), node.getPayoffP1(), node.getPayoffP2());
		}

		Iterator<GameNode> it = node.getChildren();
		GameNode tempNode;
		Result temp;


		GameNode maxNode = it.next();
		Result max = maxStrat(maxNode);

		while(it.hasNext()){

			tempNode = it.next();
			temp = maxStrat(tempNode);
			if (temp.getPayoff1() + temp.getPayoff2() > max.getPayoff1()  + max.getPayoff2()){
				maxNode = tempNode;
				max = temp;

			}else if(temp.getSize() > max.getSize())
				max.merge(temp);
		}
		max.addLabel(maxNode.getLabel());
		return max;
	}

	public static Result minMaxStrat(GameNode node){

		if(node.isTerminal()){
			return new Result(node.getLabel(), node.getPayoffP1(), node.getPayoffP2());
		}

		Iterator<GameNode> it = node.getChildren();
		GameNode tempNode;
		Result temp;

		if(node.isPlayer1()){

			GameNode maxNode = it.next();
			Result max = minMaxStrat(maxNode);

			while(it.hasNext()){
				tempNode = it.next();
				temp = minMaxStrat(tempNode);
				if (temp.getPayoff1() > max.getPayoff1() || (temp.getPayoff1() == max.getPayoff1() && temp.getPayoff2() < max.getPayoff2())){
					maxNode = tempNode;
					max = temp;

				}else if(temp.getSize() > max.getSize())
					max.merge(temp);
			}
			max.addLabel(maxNode.getLabel());
			return max;
		} else{

			GameNode minNode = it.next();
			Result min = minMaxStrat(minNode);

			while(it.hasNext()){

				tempNode = it.next();
				temp = minMaxStrat(tempNode);
				if (temp.getPayoff2() > min.getPayoff2() || (temp.getPayoff2() == min.getPayoff2() && temp.getPayoff1() < min.getPayoff1())){
					minNode = tempNode;
					min = temp;
				}else if(temp.getSize() > min.getSize())
					min.merge(temp);

				min.addLabel(minNode.getLabel());
			}
			return min;
		}
	}
}
