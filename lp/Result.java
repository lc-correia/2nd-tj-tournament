package lp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Result {
	List<String> labels;
	int payoff1;
	int payoff2;

	public Result(String label,int payoff1, int payoff2){
		labels = new ArrayList<>();
		labels.add(label);
		this.payoff1 = payoff1;
		this.payoff2 = payoff2;
	}

	public String toString(){
		Iterator<String> it = labels.iterator();
		String ret = it.next();
		while(it.hasNext())
			ret += " " + it.next();
		return ret;
	}
	
	public List<String> getLabels(){
		return labels;
	}

	public int getSize(){
		return labels.size();
	}
	public Iterator<String> getIt(){
		return labels.iterator();
	}

	public int getPayoff1(){
		return payoff1;
	}

	public int getPayoff2(){
		return payoff2;
	}

	public void addLabel(String label){
		labels.add(label);
	}

	public void merge (Result in){
		int dif = in.getSize() - labels.size();
		Iterator<String> it = (new ArrayList<String>(in.labels.subList(in.getSize() - dif, in.getSize()-1))).iterator();
		while(it.hasNext())
			labels.add(it.next());
	}
}
