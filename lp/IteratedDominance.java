package lp;

import java.util.ArrayList;
import java.util.List;

import com.winvector.linalg.DenseVec;
import com.winvector.linalg.Matrix;
import com.winvector.linalg.colt.NativeMatrix;
import com.winvector.lp.LPEQProb;
import com.winvector.lp.LPException;
import com.winvector.lp.LPException.LPMalformedException;

class NormalFormProblem
{
	List<String> rowMoves;
	List<String> colMoves;
	boolean[] pRow, pCol;
	double[][] u1, u2;
}

public class IteratedDominance {

	public static void iteratedDominanceExample() throws LPException {
		NormalFormProblem problem = exampleNormalFormProblem();	
		showNormalFormProblem(problem);
		boolean reduced = solveIterativeDomination(problem);
		while (reduced) {
			showNormalFormProblem(problem);	
			reduced = solveIterativeDomination(problem);
		}
		showNormalFormProblem(problem);				
	}
	
	public static List<boolean[]> solveIteratedDominanceStrategy(int[][] u1, int[][] u2) throws LPException {
		NormalFormProblem problem = setNormalFormProblem(u1, u2);
		boolean reduced = solveIterativeDomination(problem);
		while (reduced) 
			reduced = solveIterativeDomination(problem);
		List<boolean[]> moves = new ArrayList<boolean[]>();
		moves.add(problem.pRow);
		moves.add(problem.pCol);
		return moves;
	}
		
	public static NormalFormProblem setNormalFormProblem(int[][] u1, int[][] u2) {
		NormalFormProblem problem = new NormalFormProblem();
		problem.rowMoves = new ArrayList<String>();
		problem.colMoves = new ArrayList<String>();	

		for(int i = 1; i <= u1.length; i++)
			problem.rowMoves.add(Integer.toString(i));
		for(int i = 1; i <= u1[0].length; i++)
			problem.rowMoves.add(Integer.toString(i));
		
		boolean[] pRow = new boolean[u1.length];
		for (int i=0;i<pRow.length; i++) pRow[i] = true;
		boolean[] pCol = new boolean[u1[0].length];
		for (int j=0;j<pCol.length; j++) pCol[j] = true;

		double[][] doubleU1 = new double[u1.length][u1[0].length];

		for(int i = 0; i < u1.length; i++) {
			for(int j = 0; j < u1[0].length; j++)
				doubleU1[i][j] = u1[i][j];
		}

		double[][] doubleU2 = new double[u1.length][u1[0].length];

		for(int i = 0; i < u2.length; i++) {
			for(int j = 0; j < u2[0].length; j++)
				doubleU2[i][j] = u2[i][j];
		}

		problem.pCol = pCol;
		problem.pRow = pRow;
		problem.u1 = doubleU1;
		problem.u2 = doubleU2;

		return problem;
	}

	public static NormalFormProblem exampleNormalFormProblem(){
		NormalFormProblem problem = new NormalFormProblem();
		problem.rowMoves = new ArrayList<String>();
		problem.colMoves = new ArrayList<String>();	

		problem.rowMoves.add("1");
		problem.rowMoves.add("2");
		problem.colMoves.add("1");
		problem.colMoves.add("2");
		problem.colMoves.add("3");
		boolean[] pRow = new boolean[2];
		for (int i=0;i<pRow.length; i++) pRow[i] = true;
		boolean[] pCol = new boolean[3];
		for (int j=0;j<pCol.length; j++) pCol[j] = true;
		double[][] u2 = new double[2][3];
		u2[0][0] = 2.0; u2[0][1] = 7.0; u2[0][2] = 0.0;
		u2[1][0] = 2.0; u2[1][1] = 0.0; u2[1][2] = 8.0;
		double[][] u1 = new double[2][3];
		u1[0][0] = 8.0; u1[0][1] = 1.0; u1[0][2] = 0.0;
		u1[1][0] = 1.0; u1[1][1] = 2.0; u1[1][2] = 3.0;

		problem.pCol = pCol;
		problem.pRow = pRow;
		problem.u1 = u1;
		problem.u2 = u2;

		return problem;

	}

	public static void showNormalFormProblem(NormalFormProblem problem) {
		System.out.print("****");
		for (int j = 0; j<problem.pCol.length; j++)  if (problem.pCol[j]) 
			System.out.print("***********");
		System.out.println();
		System.out.print("  ");
		for (int j = 0; j<problem.pCol.length; j++)  if (problem.pCol[j]) {
			if (problem.colMoves.size()>0) {
				System.out.print("      ");
				System.out.print(problem.colMoves.get(j));
				System.out.print("    ");
			}
			else {
				System.out.print("\t");
				System.out.print("Col " +j);
			}
		}
		System.out.println();
		for (int i = 0; i<problem.pRow.length; i++) if (problem.pRow[i]) {
			if (problem.rowMoves.size()>0) System.out.print(problem.rowMoves.get(i)+ ": ");
			else System.out.print("Row " +i+ ": ");
			for (int j = 0; j<problem.pCol.length; j++)  if (problem.pCol[j]) {
				String fs = String.format("| %3.0f,%3.0f", problem.u1[i][j], problem.u2[i][j]);
				System.out.print(fs+"  ");
			}
			System.out.println("|");
		}
		System.out.print("****");
		for (int j = 0; j<problem.pCol.length; j++)  if (problem.pCol[j]) 
			System.out.print("***********");
		System.out.println();
	}

	public static double[][] transpose(double[][] u) {
		int lin = u.length;
		int col = u[0].length;
		double[][] t = new double[col][lin];
		for (int i = 0; i<lin; i++)
			for (int j = 0; j<col; j++) t[j][i] = u[i][j];
		return t;
	}

	public static boolean solveIterativeDomination(NormalFormProblem problem) throws LPException {
		for (int j = 0; j<problem.pCol.length; j++) if (problem.pCol[j]) {
			if (dominatedColumn(j, problem.u2, problem.pRow, problem.pCol)) {
				problem.pCol[j]=false;
				return true;
			}
		}	
		double[][] t1 = transpose(problem.u1);
		boolean[] p = new boolean[problem.pCol.length];
		p = problem.pCol;
		problem.pCol = problem.pRow;
		problem.pRow = p;
		for (int j = 0; j<problem.pCol.length; j++) if (problem.pCol[j]) {			
			if (dominatedColumn(j, t1, problem.pRow, problem.pCol)) {
				problem.pCol[j]=false;
				boolean[] b = new boolean[problem.pCol.length];
				b = problem.pCol;
				problem.pCol = problem.pRow;
				problem.pRow = b;
				return true;
			}
		}
		boolean[] a = new boolean[problem.pCol.length];
		a = problem.pCol;
		problem.pCol = problem.pRow;
		problem.pRow = a;
		return false;
	}

	public static boolean dominatedColumn(int jDom, double[][] u, boolean[] pRow, boolean[] pCol) throws LPException {

		LinearProgrammingProblem problem = new LinearProgrammingProblem();
		int nCol = 0;
		int nRow = 0;
		for (int j = 0; j<pCol.length; j++) if (pCol[j]) nCol++;
		for (int i = 0; i<pRow.length; i++) if (pRow[i]) nRow++;
		problem.numVariables = nCol - 1;   // number of variables
		problem.numConstraints = nRow + 1; // number of constraints
		final Matrix<NativeMatrix> m = NativeMatrix.factory.newMatrix(problem.numConstraints,problem.numVariables+problem.numConstraints,false);
		final double[] b = new double[problem.numConstraints];
		final double[] c = new double[problem.numVariables+problem.numConstraints];

		int row = 0;
		int column = 0;

		for(int i = 0; i < pRow.length; i++) {
			if(pRow[i]) {
				column = 0;
				for(int j = 0; j < pCol.length; j++) {
					if(pCol[j] && j != jDom) {
						m.set(row, column, u[i][j]);
						column++;
					}
				}
				m.set(row, column+row, -1.0);
				b[row] = u[i][jDom];
				row++;

			}
		}

		for(int j = 0; j < problem.numVariables; j++) {
			m.set(row, j, 1.0);
			c[j] = 1.0;
		}
		b[row] = 1.0;

		problem.m = m;
		problem.b = b;
		problem.c = c;
		problem.prob = null;
		try {
			problem.prob = new LPEQProb(m.columnMatrix(),b,new DenseVec(c));
		}
		catch (LPMalformedException e) {
			System.out.println("Error in problem specification!");
			e.printStackTrace();
		}
		LinearProgramming.solveProblem(problem);

		if (problem.feasible) {
			if (problem.bounded) {
				double[] s = LinearProgramming.getSlacks(problem);
				for(int i = 0; i < s.length; i++) {
					if(s[i] > 0)
						return true;
				}
			}
		}
		return false;
	}
}




