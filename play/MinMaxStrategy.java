package play;

import java.util.Iterator;
import java.util.List;

import lp.MinMax;
import play.exception.InvalidStrategyException;

public class MinMaxStrategy extends Strategy{

	boolean calculated;
	List<String> labels;
	
	@Override
	public void execute() throws InterruptedException {

		
		while(!this.isTreeKnown()) {
			System.err.println("Waiting for game tree to become available.");
			Thread.sleep(1000);
		}

		while(true) {

			PlayStrategy myStrategy = this.getStrategyRequest();
			if(myStrategy == null) //Game was terminated by an outside event
				break;	
			if(!calculated){
				labels = MinMax.minMaxStrat(tree.getRootNode()).getLabels();
			}
			Iterator<String> it = labels.iterator();

			while(it.hasNext()){
				myStrategy.put(it.next(), 1.0);
				
			}
			try{
				this.provideStrategy(myStrategy);
			} catch (InvalidStrategyException e) {
				System.err.println("Invalid strategy: " + e.getMessage());;
				e.printStackTrace(System.err);
			} 
		}
	}
}