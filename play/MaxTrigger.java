package play;

import java.util.Iterator;
import java.util.List;

import gametree.GameNode;
import lp.MinMax;
import lp.Result;
import play.exception.InvalidStrategyException;

public class MaxTrigger extends Strategy{

	boolean calculated;
	boolean triggered;
	int expectedP1;
	int expectedP2;
	List<String> labels;
	
	@Override
	public void execute() throws InterruptedException {

		
		while(!this.isTreeKnown()) {
			System.err.println("Waiting for game tree to become available.");
			Thread.sleep(1000);
		}

		while(true) {

			PlayStrategy myStrategy = this.getStrategyRequest();
			if(myStrategy == null) //Game was terminated by an outside event
				break;	
			
			
			
			if(!calculated && !triggered){
				Result last = MinMax.maxStrat(tree.getRootNode());
				expectedP1 = last.getPayoff1();
				expectedP2 = last.getPayoff2();
				labels = last.getLabels();
				calculated = true;
				
			}else if (calculated && !triggered){
				
				GameNode finalP1 = this.tree.getNodeByIndex(myStrategy.getFinalP1Node());
				GameNode finalP2 = this.tree.getNodeByIndex(myStrategy.getFinalP2Node());
				
				if (finalP1.getPayoffP1() != expectedP1 || finalP2.getPayoffP2() != expectedP2){
					triggered = true;
					labels = MinMax.minMaxStrat(tree.getRootNode()).getLabels();
				}
			}
			
			Iterator<String> it = labels.iterator();

			while(it.hasNext()){
				myStrategy.put(it.next(), 1.0);
				
			}
			try{
				this.provideStrategy(myStrategy);
			} catch (InvalidStrategyException e) {
				System.err.println("Invalid strategy: " + e.getMessage());;
				e.printStackTrace(System.err);
			} 
		}
	}
}
