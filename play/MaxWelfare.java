package play;

import java.util.Iterator;
import java.util.List;

import lp.*;
import play.exception.InvalidStrategyException;

public class MaxWelfare extends Strategy{

	boolean calculated;
	List<String> labels;

	@Override
	public void execute() throws InterruptedException {

		while(!this.isTreeKnown()) {
			System.err.println("Waiting for game tree to become available.");
			Thread.sleep(1000);
		}

		while(true) {

			PlayStrategy myStrategy = this.getStrategyRequest();
			if(myStrategy == null) //Game was terminated by an outside event
				break;	


			int totalRoundsLeft = myStrategy.getMaximumNumberOfIterations();
			if(!calculated){

				labels = MinMax.maxStrat(tree.getRootNode()).getLabels();
				calculated = true;
			}else if (totalRoundsLeft == 1)
				labels = MinMax.minMaxStrat(tree.getRootNode()).getLabels();


			Iterator<String> it = labels.iterator();
			while(it.hasNext()){
				myStrategy.put(it.next(), 1.0);
			}

			try{
				this.provideStrategy(myStrategy);
			} catch (InvalidStrategyException e) {
				System.err.println("Invalid strategy: " + e.getMessage());;
				e.printStackTrace(System.err);
			} 
		}
	}
}
